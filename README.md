# HTTP Serv

A simple HTTP file server written in Go. I could have just used
`python -m SimpleHTTPServer` but where is the fun in that?

It uses `html/template` but has an internal template so all you have to do is
run the binary and you're good to go.

## Installing

Run ```go install github.com/antizealot1337/httpserv```

## Running the HTTP file server

It currently accepts a few different command line arguments.

* -root: This is the directory you want to server (defaults to ".")
* -port: The port you want the server to run on (defaults to 8080).
* -a: Shows hidden files. A hidden file is a file that begins with a ".".

## License
Copyright 2016, 2017

All files in this repository are licensed under the terms of the MIT license.
See the LICENSE file for more information.
