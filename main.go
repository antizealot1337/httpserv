package main

import (
	"flag"
	"log"
	"net/http"
	"strconv"
)

func main() {
	// The root directory for the web server
	var rootDir string

	// The port to run the server on
	var port uint

	// Determines if hidden files are displayed or not
	var showHidden bool

	// Setup the flags
	flag.StringVar(&rootDir, "root", ".", "The root directory for the server")
	flag.UintVar(&port, "port", 8080, "The port to run the server")
	flag.BoolVar(&showHidden, "a", false, "Show all(hidden) files")

	// Parse the arguments
	flag.Parse()

	// Expand the root directory
	rootDir = expandRoot(rootDir)

	log.Println("Server Root:", rootDir)

	// Create a sharer and let it handle all requests
	http.Handle("/", newSharer(rootDir, dirTemplate, showHidden))

	log.Println("Server Port:", port)

	log.Println("Starting server")

	// Start handling requests
	log.Fatal(http.ListenAndServe(":"+strconv.FormatUint(uint64(port), 10), nil))
} //main
