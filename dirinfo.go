package main

import (
	"os"
	"path"
	"sort"
)

type entry struct {
	Name string
	Path string
	Size int64
} //struct

func newEntry(dir string, file os.FileInfo) entry {
	var e entry

	e.Path = path.Join(dir, file.Name())
	e.Size = file.Size()

	if file.IsDir() {
		e.Name = file.Name() + "/"
	} else {
		e.Name = file.Name()
	} //if

	return e
} //newEntry

type entrySlice []entry

func (e entrySlice) Len() int {
	return len(e)
} //Len

func (e entrySlice) Less(i, j int) bool {
	return e[i].Name < e[j].Name
} //Less

func (e entrySlice) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
} //Swap

type dirInfo struct {
	Path    string
	Parent  string
	Entries entrySlice
} //struct

func buildDirInfo(reqPath string, dir *os.File, showHidden bool) (*dirInfo, error) {
	// Get the entries
	entries, err := gatherEntires(reqPath, dir, showHidden)

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	// Return the directory info
	return &dirInfo{
		Path:    reqPath,
		Parent:  path.Dir(reqPath),
		Entries: entries,
	}, nil
} //buildDirInfo

func gatherEntires(reqPath string, dir *os.File, showHidden bool) (entries entrySlice, err error) {
	// Attempt to read the directory's contents
	contents, err := dir.Readdir(-1)

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	// Loop through the contents
	for _, f := range contents {
		// Check if we are excluding the hidden files
		if !showHidden {
			// Check if this file is hidden
			if []rune(f.Name())[0] == '.' {
				continue
			} //if
		} //if

		// Add the entry to the slice
		entries = append(entries, newEntry(reqPath, f))
	} //for

	// Sort the entries
	sort.Sort(entries)

	return
} //gatherEntries
