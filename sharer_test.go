package main

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"strconv"
	"testing"
)

func TestNewSharer(t *testing.T) {
	// The root for the sharer
	root := "/root"

	// Create an empty templater
	tmpl := template.Must(template.New("root").Parse(""))

	// Create a sharer
	s := newSharer(root, tmpl, true)

	// Make sure it isn't nil
	if s == nil {
		t.Fatal("Did not expect the sharer to be nil")
	} //if

	// Check the root
	if expected, actual := root, s.root; actual != expected {
		t.Errorf(`Expected the root to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the templater
	if expected, actual := tmpl, s.tmpl; actual != expected {
		t.Error("Incorrect templater")
	} //if

	// Make sure hidden files are included according to the sharer
	if !s.showHidden {
		t.Error("Expected to be showing hidden files")
	} //if
} //TestNewSharer

func TestSharerFileError(t *testing.T) {
	// The buffer for logging
	var out bytes.Buffer

	// Create a sharer
	s := &sharer{
		log: log.New(&out, "", 0),
	}

	// Create a request for logging
	req := httptest.NewRequest("GET", "http://localhost/test", nil)

	// A Recorder to record the response
	rec := httptest.NewRecorder()

	// The return the code
	retCode := http.StatusNotFound

	// Indicate a file error occured
	s.fileErr(rec, req, os.ErrNotExist, retCode)

	// Create the expected string for the log
	logStr := req.RemoteAddr + " " + req.URL.Path + " " + strconv.Itoa(retCode) +
		"\n"

	// Check the buffer
	if expected, actual := logStr, out.String(); actual != expected {
		t.Errorf(`Expected to log "%s" but was "%s"`, expected, actual)
	} //if

	// Check the return code
	if expected, actual := retCode, rec.Code; actual != expected {
		t.Errorf("Expected the status code to be %d but was %d", expected, actual)
	} //if

	// Create the expected content
	content := os.ErrNotExist.Error() + "\n"

	// Check the returned value
	if expected, actual := content, rec.Body.String(); actual != expected {
		t.Errorf(`Expected the content to be "%s" but was "%s"`, expected, actual)
	} //if
} //TestSharerFileError

func TestSharerLogReqStatus(t *testing.T) {
	// The buffer for logging
	var out bytes.Buffer

	// Create a sharer
	s := &sharer{
		log: log.New(&out, "", 0),
	}

	// Create a request for logging
	req := httptest.NewRequest("GET", "http://localhost/test", nil)

	// Log the request
	s.logReqStatus(req, http.StatusOK)

	// Create the expected string for the log
	logStr := req.RemoteAddr + " " + req.URL.Path + " " +
		strconv.Itoa(http.StatusOK) + "\n"

	// Check the buffer
	if expected, actual := logStr, out.String(); actual != expected {
		t.Errorf(`Expected to log "%s" but was "%s"`, expected, actual)
	} //if
} //TestSharerLogReqStatus

func TestExpandRoot(t *testing.T) {
	// Get the current directory
	cd, err := os.Getwd()

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Check a path that doesn't need expanding
	if expected, actual := "/", expandRoot("/"); actual != expected {
		t.Errorf(`Expected expanded path to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check expansion for the current directory
	if expected, actual := cd, expandRoot("."); actual != expected {
		t.Errorf(`Expected expanded path to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check expansion for the parent directory
	if expected, actual := path.Dir(cd), expandRoot(".."); actual != expected {
		t.Errorf(`Expected expanded path to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check expansion for a different subdirectory of the parent
	if expected, actual := path.Join(cd, "test"), expandRoot("./test"); actual != expected {
		println("Fails here")
		t.Errorf(`Expected expanded path to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check expansion for a different subdirectory of the parent
	if expected, actual := path.Join(path.Dir(cd), "test"), expandRoot("../test"); actual != expected {
		t.Errorf(`Expected expanded path to be "%s" but was "%s"`,
			expected, actual)
	} //if
} //TestExpandRoot
