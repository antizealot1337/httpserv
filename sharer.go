package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"path"
)

type templater interface {
	Execute(out io.Writer, data interface{}) error
} //templater

type sharer struct {
	root       string
	tmpl       templater
	log        *log.Logger
	showHidden bool
} //struct

func newSharer(root string, tmpl templater, showHidden bool) *sharer {
	return &sharer{
		root:       root,
		tmpl:       tmpl,
		log:        log.New(os.Stdout, "", log.LstdFlags),
		showHidden: showHidden,
	}
} //newSharer

// ServeHTTP serves a file or HTML describing the directory request upon sucess.
func (s *sharer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// Create the request path
	reqPath := path.Join(s.root, r.URL.Path)

	// Attempt to open the request path
	reqFile, err := os.Open(reqPath)

	// Check for an error
	if err != nil {
		s.fileErr(w, r, err, http.StatusNotFound)

		// End the request
		return
	} //if

	// Make sure the file is closed after the request
	defer reqFile.Close()

	// Stat the file
	stat, err := reqFile.Stat()

	// Check for an error
	if err != nil {
		s.fileErr(w, r, err, http.StatusForbidden)

		// End the request
		return
	} //if

	// Log an OK status
	s.logReqStatus(r, http.StatusOK)

	// Check if the file is a directory
	if stat.IsDir() {
		// Generate the directory information
		dir, _ := buildDirInfo(r.URL.Path, reqFile,
			s.showHidden)

		// Render the template
		err = s.tmpl.Execute(w, dir)
	} else {
		http.ServeContent(w, r, stat.Name(), stat.ModTime(), reqFile)
	} //if

	// Check to see if an error occured after the response started
	if err != nil {
		s.log.Printf("ERROR: %s %s %s", err.Error(), r.RemoteAddr,
			r.URL.Path)
	} //if
} //ServeHTTP

func (s *sharer) fileErr(w http.ResponseWriter, r *http.Request, err error, code int) {
	// Respond with an error
	http.Error(w, err.Error(), code)

	// Log the error
	s.logReqStatus(r, code)
} //fileErr

func (s *sharer) logReqStatus(r *http.Request, code int) {
	s.log.Printf("%s %s %d", r.RemoteAddr, r.URL.Path, code)
} //logError

func expandRoot(root string) string {
	if rroot := []rune(root); rroot[0] == '.' {
		// Get the current directory
		cd, err := os.Getwd()

		// Check for an error
		if err != nil {
			panic(err)
		} //if

		// Check if the path is simple
		switch root {
		case ".":
			return cd
		case "..":
			return path.Dir(cd)
		} //switch

		// Check if the second is also a '.'
		if rroot[1] == '.' {
			return path.Clean(path.Join(path.Dir(cd), root[2:]))
		} //if

		// Return the current directory and everything after the first '.'
		return path.Clean(path.Join(cd, root[1:]))
	} //if

	return path.Clean(root)
} //expandRoot
