package main

import (
	"os"
	"path"
	"sort"
	"testing"
)

var _ sort.Interface = (entrySlice)(nil)

func TestNewEntry(t *testing.T) {
	// The root for the entry
	root, filePath := "testdata", "entryTest0"

	// Open a file
	file, err := os.Open(path.Join(root, filePath))

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Make sure the file is closed
	defer file.Close()

	// Get the info
	fileInfo, err := file.Stat()

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Create an entry
	entry := newEntry(root, fileInfo)

	// Check the name of the entry
	if expected, actual := "entryTest0", entry.Name; actual != expected {
		t.Errorf(`Expected the name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the path for the entry
	if expected, actual := path.Join(root, filePath), entry.Path; actual != expected {
		t.Errorf(`Expected the path to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the size
	if expected, actual := fileInfo.Size(), entry.Size; actual != expected {
		t.Errorf("Expected the size to be %d but was %d", actual, expected)
	} //if
} //TestNewEntry

func TestEntrySliceLen(t *testing.T) {
	// The entires
	var e entrySlice

	// Check the length
	if expected, actual := 0, e.Len(); actual != expected {
		t.Errorf("Expected the length to be %d but was %d", expected, actual)
	} //if

	// Create a slice of entries
	e = entrySlice{
		entry{Name: "B"},
		entry{Name: "A"},
	}

	// Check the length
	if expected, actual := len(e), e.Len(); actual != expected {
		t.Errorf("Expected the length to be %d but was %d", expected, actual)
	} //if
} //TestEntrySliceLen

func TestEntrySliceLess(t *testing.T) {
	// Create a slice of entries
	e := entrySlice{
		entry{Name: "B"},
		entry{Name: "A"},
	}

	// Check if the first is less than the second
	if i, j := 0, 1; e.Less(i, j) {
		t.Errorf("Expected %d to not be less than %d", i, j)
	} //if

	// Check if the first is less than the second
	if i, j := 1, 0; !e.Less(i, j) {
		t.Errorf("Expected %d to be less than %d", i, j)
	} //if
} //TestEntrySliceLess

func TestEntruSliceSwap(t *testing.T) {
	// Some entries
	a, b := entry{Name: "a"}, entry{Name: "b"}

	// Create a slice of entries
	e := entrySlice{
		a,
		b,
	}

	// Attempt to swap the first and second
	e.Swap(0, 1)

	// Check the first entry
	if expected, actual := b.Name, e[0].Name; actual != expected {
		t.Errorf(`Expected the first entry name to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the second entry
	if expected, actual := a.Name, e[1].Name; actual != expected {
		t.Errorf(`Expected the second entry name to be "%s" but was "%s"`, expected,
			actual)
	} //if
} //TestEntrySliceSwapp

func TestBuildDirInfo(t *testing.T) {
	// The directory to test
	testDir := "testdata/buildDirInfo0"

	// Open the file
	file, err := os.Open(testDir)

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Make sure the file is closed
	defer file.Close()

	// Build the info
	dir, err := buildDirInfo(testDir, file, false)

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Check the path
	if expected, actual := testDir, dir.Path; actual != expected {
		t.Errorf(`Expected the path to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the parent
	if expected, actual := path.Dir(testDir), dir.Parent; actual != expected {
		t.Errorf(`Expected the parent to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check the number of entries
	if expected, actual := 1, len(dir.Entries); actual != expected {
		t.Errorf("Expected the number of entries to be %d but was %d",
			expected, actual)
	} //if

	// Re-open the file
	file, _ = os.Open(testDir)

	// Make sure the file is closed
	defer file.Close()

	// Build the info
	dir, err = buildDirInfo(testDir, file, true)

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Check the path
	if expected, actual := testDir, dir.Path; actual != expected {
		t.Errorf(`Expected the path to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the parent
	if expected, actual := path.Dir(testDir), dir.Parent; actual != expected {
		t.Errorf(`Expected the parent to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check the number of entries
	if expected, actual := 2, len(dir.Entries); actual != expected {
		t.Errorf("Expected the number of entries to be %d but was %d",
			expected, actual)
	} //if

	// REVIEW: Make sure this isn't testing a logic error

	// Re-open the file
	file, _ = os.Open(testDir)

	// Make sure the file is closed
	defer file.Close()

	// Get the current working directory
	cd, err := os.Getwd()

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Build the info
	dir, err = buildDirInfo(path.Join(cd, testDir), file, true)

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Check the path
	if expected, actual := path.Join(cd, testDir), dir.Path; actual != expected {
		t.Errorf(`Expected the path to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the parent
	if expected, actual := path.Dir(path.Join(cd, testDir)), dir.Parent; actual != expected {
		t.Errorf(`Expected the parent to be "%s" but was "%s"`,
			expected, actual)
	} //if

	// Check the number of entries
	if expected, actual := 2, len(dir.Entries); actual != expected {
		t.Errorf("Expected the number of entries to be %d but was %d",
			expected, actual)
	} //if
} //TestBuildDirInfo

func TestGatherEntries(t *testing.T) {
	// The directory to test
	rootPath := "testdata/gatherEntries0"

	// Open the directory
	rootDir, err := os.Open(rootPath)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err)
	} //if

	// Gather the entries
	entries, err := gatherEntires("", rootDir, true)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err)
	} //if

	// Check the number of entries
	if expected, actual := 4, len(entries); actual != expected {
		t.Errorf("Expected %d entries but there were %d", expected, actual)
	} //if

	// Close the file
	rootDir.Close()

	// Open the directory
	rootDir, err = os.Open(rootPath)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err)
	} //if

	defer rootDir.Close()

	// Gather the entries
	entries, err = gatherEntires("", rootDir, false)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err)
	} //if

	// Check the number of entries
	if expected, actual := 3, len(entries); actual != expected {
		t.Errorf("Expected %d entries but there were %d", expected, actual)
	} //if
} //TestGatherEntries
