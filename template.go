package main

import "html/template"

const dirTemplateSrc = `<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width initial-scale=1">
    <title>{{.Path}}</title>
  </head>
  <body>
    <h1>Directory {{.Path}}</h1>
{{if .Parent}}
    <div>Goto <a href="{{.Parent}}">Parent</a></div>
{{end}}
    <table>
      <thead>
				<th>Filename</th>
				<th>Size</th>
      </thead>
      <tbody>
{{range $file := .Entries}}
			<tr>
				<td><a href="{{$file.Path}}">{{$file.Name}}</a></td>
				<td>{{$file.Size}}</td>
			</tr>
{{end}}
      </tbody>
    </table>
  </body>
</html>`

var dirTemplate *template.Template

func init() {
	// Compile the directory template
	dirTemplate = template.Must(template.New("dir").Parse(dirTemplateSrc))
} //init
